#!/bin/bash

# Script abort on error
set -e

# Expected Settings via environment variables:
# COMPILER= gcc/clang
# LANGUAGE= C++98 / C++11


#include ci options script
MY_DIR=$(dirname $(readlink -f $0))
source $MY_DIR/ci-linux-config.sh

echo "Building with path: build-release-$BUILDPATH"
echo "Full cmake options: $OPTIONS  "


#########################################
# Build Release version and Unittests
#########################################

# Make release build folder
if [ ! -d build-release-$BUILDPATH ]; then
  mkdir build-release-$BUILDPATH
fi

cd build-release-$BUILDPATH

cmake -DCMAKE_BUILD_TYPE=Release -DSTL_VECTOR_CHECKS=ON $OPTIONS ../

#build it
make $MAKE_OPTIONS

# copy the used shared libraries to the lib folder
#mkdir systemlib
#ldd GlowApp | grep "=> /" | awk '{print $3}' | xargs -I '{}' cp -v '{}' systemlib
#cd ..
