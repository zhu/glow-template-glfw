########################################
# Build
########################################

if [ ! -d build-release ]; then
  mkdir build-release
fi

cd build-release

# Build without ports to avoid qt4 collision
/opt/local/bin/cmake ../ -DCMAKE_CXX_FLAGS='-std=c++11'

make
