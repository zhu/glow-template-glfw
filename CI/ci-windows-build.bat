@echo off
:: clone libraries git (set env variable to GIT_SSH_COMMAND maybe use setx once as this key won't change) 
set GIT_SSH_COMMAND=ssh -i E:\\\gitlab\\\id_rsa 

::load configuration and settings
call %~dp0\ci-windows-config.bat

mkdir rel
cd rel

::clear any old binary files
del *.exe

::invoke cmake
"C:\Program Files\CMake\bin\cmake.exe" -G "%GENERATOR%"  -DCMAKE_BUILD_TYPE=Release ..

IF %errorlevel% NEQ 0 exit /b %errorlevel%

:: build

%VS_PATH% /Build "Release" GlowApp.sln /Project "ALL_BUILD"

IF %errorlevel% NEQ 0 exit /b %errorlevel%
