#include "GlowApp.hh"

int main(int argc, char *argv[])
{
    GlowApp app;
    return app.run(argc, argv); // automatically sets up GLOW and GLFW and everything
}
